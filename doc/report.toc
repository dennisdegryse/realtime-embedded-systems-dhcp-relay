\contentsline {section}{\numberline {1}DHCP Relay}{2}
\contentsline {subsection}{\numberline {1.1}DORA}{2}
\contentsline {subsection}{\numberline {1.2}Functionality}{2}
\contentsline {paragraph}{Change broadcast to unicast}{2}
\contentsline {paragraph}{Place SIADDR (Server IP Address)}{2}
\contentsline {paragraph}{Place GIADDR (Gateway IP Address)}{2}
\contentsline {paragraph}{}{2}
\contentsline {section}{\numberline {2}Structural choices}{2}
\contentsline {subsection}{\numberline {2.1}Realtime scenarios}{4}
\contentsline {paragraph}{Multiple clients boot and server response}{4}
\contentsline {paragraph}{Incorrect infinite DHCP packet}{4}
\contentsline {paragraph}{Time constrained execution}{4}
\contentsline {subsection}{\numberline {2.2}ASG Diagram}{4}
\contentsline {section}{\numberline {3}DHCP Relay Task}{5}
\contentsline {section}{\numberline {4}Test Procedure}{5}
