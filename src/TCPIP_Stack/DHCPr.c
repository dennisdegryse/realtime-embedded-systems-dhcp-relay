/*********************************************************************
 *
 *  Dynamic Host Configuration Protocol (DHCP) Relay
 *  Module for Microchip TCP/IP Stack
 *     -Acts as an intermediate relay for DHCP requests over multiple
 *      subnets.
 *     -Reference: RFC 2131, 2132
 *
 *********************************************************************
 * FileName:        DHCPr.c
 * Dependencies:    UDP, ARP, Tick
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *                    Microchip C30 v3.12 or higher
 *                    Microchip C18 v3.30 or higher
 *                    HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *        ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *        used in conjunction with a Microchip ethernet controller for
 *        the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     02/28/07    Original
 ********************************************************************/
#define __DHCPS_C
#define __18F97J60
#define __SDCC__
#include <pic18f97j60.h> //ML

#include "../Include/TCPIPConfig.h"


#if defined(STACK_USE_DHCP_RELAY)

#include "../Include/TCPIP_Stack/TCPIP.h"
#define NUM_ENDPOINTS 2
#define CLIENT_SUBNET_ENDPOINT { DHCP_SERVER_PORT, DHCP_CLIENT_PORT, INVALID_UDP_SOCKET, DHCP_OPEN_SOCKET }
#define SERVER_SUBNET_ENDPOINT { DHCP_CLIENT_PORT, DHCP_SERVER_PORT, INVALID_UDP_SOCKET, DHCP_OPEN_SOCKET }
static ENDPOINT  Endpoints[NUM_ENDPOINTS] = { CLIENT_SUBNET_ENDPOINT, SERVER_SUBNET_ENDPOINT };

static void DHCPRelayEndpointTask(ENDPOINT *srcEndpoint, ENDPOINT *destEndpoint);
static BOOL DHCPOpenEndpoint(ENDPOINT *endpoint);

/*****************************************************************************
  Function:
    void DHCPRelayTask(void)

  Summary:
    Performs periodic DHCP relay tasks.

  Description:
    This function performs any periodic tasks requied by the DHCP relay 
    module, such as relaying DHCP requests and responses.

  Precondition:
    None

  Parameters:
    None

  Returns:
      None
  ***************************************************************************/
void DHCPRelayTask(void)
{
    int i;

    for (i = 0; i < NUM_ENDPOINTS; i++)
        DHCPRelayEndpointTask(&(Endpoints[i]), &(Endpoints[i + 1 % NUM_ENDPOINTS]));
}

/*****************************************************************************
  Function:
    void DHCPRelayEndpointTask(void)

  Summary:
    Performs periodic DHCP server tasks.

  Description:
    This function performs any periodic tasks requied by the DHCP server 
    module, such as processing DHCP requests and distributing IP addresses.

  Precondition:
    None

  Parameters:
    srcEndpoint - The endpoint to read packets from
    destEndpoint - The endpoint to write packets to

  Returns:
      None
  ***************************************************************************/
static void DHCPRelayEndpointTask(ENDPOINT *srcEndpoint, ENDPOINT *destEndpoint)
{
    int         i;
    DHCP_PACKET DHCPPacket;
    BYTE        Option, Len, Value;
    WORD        PacketSize;
    BYTE        Options1[256];
    BYTE        Options2[DHCP_OPTIONS_LENGTH - 256];

    DHCPPacket.OptionsLength = 0;

    // Read the next packet from the source endpoint and relay it to the destination endpoint
    switch(srcEndpoint->status)
    {
    case DHCP_OPEN_SOCKET:
        if(!DHCPOpenEndpoint(srcEndpoint))
            break;

    case DHCP_LISTEN:
        // Check to see if a valid DHCP packet has arrived
        if(UDPIsGetReady(srcEndpoint->socket) <= DHCP_MIN_LENGTH)
            break;

        // Retrieve the BOOTP header
        if(!UDPGetArray((BYTE*)&DHCPPacket, DHCP_MIN_LENGTH))
            break;

        if(DHCPPacket.MagicCookie != DHCP_MAGIC_COOKIE)
            break;

        // if messageType == 1 = BOOTREQUEST, 2 = BOOTREPLY
        // 1) Sent to server or
        // 2) Sent to client
        if(DHCPPacket.BOOTPHeader.MessageType == 1)
        {
            // Set DHCP Server IP address
            DHCPPacket.BOOTPHeader.NextServerIP.Val = AppConfig.DHCPServer.Val;

            // Set Relay Agent IP address
            DHCPPacket.BOOTPHeader.RelayAgentIP.Val = AppConfig.MyIPAddr.Val;
        }

        // Obtain options
        while(1)
        {
            // Get option type
            if(!UDPGet(&Option))
                break;

            if (DHCPPacket.OptionsLength < 256)
                Options1[DHCPPacket.OptionsLength++] = Option;
            else
                Options2[DHCPPacket.OptionsLength++ - 256] = Option;

            if(Option == DHCP_END_OPTION)
                break;

            // Get option length
            UDPGet(&Len);

            if (DHCPPacket.OptionsLength < 256)
                Options1[DHCPPacket.OptionsLength++] = Len;
            else
                Options2[DHCPPacket.OptionsLength++ - 256] = Len;

            while (Len--)
                if (DHCPPacket.OptionsLength < 256)
                    UDPGet((CHAR *)&(Options1[DHCPPacket.OptionsLength++]));
                else
                    UDPGet((CHAR *)&(Options2[DHCPPacket.OptionsLength++ - 256]));
        }

        UDPDiscard();

        // Make sure the destination endpoint is ready to be written to
        if(destEndpoint->status == DHCP_OPEN_SOCKET)
            if (!DHCPOpenEndpoint(destEndpoint))
                break;

        if(UDPIsPutReady(destEndpoint->socket) < DHCP_MIN_LENGTH)
            break;
        
        // Send the relayed packet
        UDPPutArray((BYTE*)&DHCPPacket, DHCP_MIN_LENGTH);

        for (i = 0; i < DHCPPacket.OptionsLength; i++)
            if (i < 256)
                UDPPut(Options1[i]);
            else
                UDPPut(Options2[i - 256]);

        UDPFlush();

        break;
    }
}

/*****************************************************************************
  Function:
    BOOL DHCPOpenEndpoint(ENDPOINT *endpoint)

  Summary:
    Opens the UDP socket at the given endpoint

  Description:
    This function initializes the UDP socket connection for the endpoint.

  Precondition:
    None

  Parameters:
    endpoint - the endpoint at which a socket must be opened

  Returns:
      None
  ***************************************************************************/
static BOOL DHCPOpenEndpoint(ENDPOINT *endpoint) {
    // Obtain a UDP socket to listen/transmit on
    endpoint->socket = UDPOpen(endpoint->localPort, NULL, endpoint->remotePort);

    if(endpoint->socket == INVALID_UDP_SOCKET)
        return FALSE;

    endpoint->status ++;

    return TRUE;
}

#endif // defined(STACK_USE_DHCP_RELAY)

